package test.ru.currentsmile.presentationlayer.presenter.main

import androidx.recyclerview.widget.DiffUtil.DiffResult
import test.ru.currentsmile.presentationlayer.presenter.base.BaseMvpView

interface IMain : BaseMvpView {
    fun updateState(hintText: String)
    fun updateList(diffResult: DiffResult)
    fun clearSearch()
}