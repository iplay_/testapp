package test.ru.currentsmile.presentationlayer.utils.extension

import android.content.Context
import android.graphics.Color
import android.os.Build
import android.view.View
import androidx.core.content.ContextCompat
import androidx.fragment.app.Fragment
import com.google.android.material.snackbar.Snackbar
import test.ru.currentsmile.R

fun Context.color(colorRes: Int) = ContextCompat.getColor(this, colorRes)

fun View.visible(visible: Boolean) {
    this.visibility = if (visible) View.VISIBLE else View.GONE
}

fun View.showSnack(message: String) {
    Snackbar.make(this, message, Snackbar.LENGTH_LONG).show()
}

fun Fragment.changeStatusBar(isVisible: Boolean) {
    activity?.let {
        if (isVisible) {
            it.window.decorView.systemUiVisibility = View.SYSTEM_UI_FLAG_LAYOUT_STABLE
            it.window.statusBarColor = it.color(R.color.colorPrimary)
        } else {
            it.window.decorView.systemUiVisibility =
                View.SYSTEM_UI_FLAG_LAYOUT_STABLE or View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN or View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                it.window.statusBarColor = Color.TRANSPARENT
            }
        }
    }
}