package test.ru.currentsmile.presentationlayer.navigation

import androidx.fragment.app.Fragment
import ru.terrakok.cicerone.android.support.SupportAppScreen
import test.ru.currentsmile.presentationlayer.ui.main.MainFragment

object Screens {

    class MainScreen : SupportAppScreen() {
        override fun getFragment(): Fragment {
            return MainFragment()
        }
    }

}