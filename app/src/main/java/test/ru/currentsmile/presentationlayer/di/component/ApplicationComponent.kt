package test.ru.currentsmile.presentationlayer.di.component

import dagger.Component
import test.ru.currentsmile.presentationlayer.di.module.ContextModule
import test.ru.currentsmile.presentationlayer.di.module.NavigationModule
import test.ru.currentsmile.presentationlayer.di.module.NetworkModule
import test.ru.currentsmile.presentationlayer.di.module.RepositoryModule
import test.ru.currentsmile.presentationlayer.presenter.main.MainPresenter
import test.ru.currentsmile.presentationlayer.ui.base.BaseActivity
import javax.inject.Singleton

@Singleton
@Component(
    modules = [(ContextModule::class), (NetworkModule::class), (RepositoryModule::class),
        (NavigationModule::class)]
)
interface ApplicationComponent {

    /* Activities  */
    fun inject(baseActivity: BaseActivity)

    /* Presenters */
    fun inject(mainPresenter: MainPresenter)

    //Можно это все раскидать по модульно, но мало времени :)


}