package test.ru.currentsmile.presentationlayer.model.diff

import androidx.recyclerview.widget.DiffUtil
import test.ru.currentsmile.presentationlayer.model.main.CitySealedModel

class CityDiffUtilCallback constructor(private val oldList: List<CitySealedModel>,
    private val newList: List<CitySealedModel>)
    : DiffUtil.Callback() {

    override fun areItemsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old = oldList[oldItemPosition]
        val new = newList[newItemPosition]
        return if (old is CitySealedModel.Title && new is CitySealedModel.Title) {
            old.title == new.title
        } else {
            (old as CitySealedModel.CityModel).id == (new as CitySealedModel.CityModel).id
        }
    }

    override fun areContentsTheSame(oldItemPosition: Int, newItemPosition: Int): Boolean {
        val old = oldList[oldItemPosition]
        val new = newList[newItemPosition]
        return if (old is CitySealedModel.Title && new is CitySealedModel.Title) {
            old.title == new.title
        } else {
            (old as CitySealedModel.CityModel).id == (new as CitySealedModel.CityModel).id
                    && old.city == new.city && old.hotels == new.hotels
        }
    }

    override fun getOldListSize() = oldList.size
    override fun getNewListSize() = newList.size
}