package test.ru.currentsmile.presentationlayer.model.mapper

import test.ru.currentsmile.R
import test.ru.currentsmile.domainlayer.entity.response.CityEntity
import test.ru.currentsmile.presentationlayer.model.main.CitySealedModel
import test.ru.currentsmile.presentationlayer.model.main.CitySealedModel.CityModel
import test.ru.currentsmile.presentationlayer.utils.base.StringProvider

class MainMapper(private val stringProvider: StringProvider) {

    fun map(list: List<CityEntity>, isDestination: Boolean) : List<CitySealedModel> {
        val listOfCities = arrayListOf<CitySealedModel>()

        listOfCities.add(CitySealedModel.Title(
            if (isDestination) {
                stringProvider.getString(R.string.main_select_destination)
            } else {
                stringProvider.getString(R.string.main_select_origin)
            }
        ))

        for (cityEntity in list) {
            val id = cityEntity.id
            val name = cityEntity.fullname
            val city = cityEntity.city
            val location = cityEntity.location
            val hotels = String.format(stringProvider.getString(R.string.hotels_count),
                cityEntity.hotelsCount.toString())
            listOfCities.add(CityModel(id, name, city, location, hotels))
        }

        return listOfCities
    }

}