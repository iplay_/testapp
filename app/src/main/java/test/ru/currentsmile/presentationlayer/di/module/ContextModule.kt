package test.ru.currentsmile.presentationlayer.di.module

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides
import test.ru.currentsmile.presentationlayer.di.scope.AppScope
import test.ru.currentsmile.presentationlayer.utils.base.StringProvider
import javax.inject.Singleton

@AppScope
@Module
@Suppress("unused")
class ContextModule(private val app: Application) {

    @Provides
    @Singleton
    fun provideContext(): Context = app.applicationContext

    @Provides
    @Singleton
    fun provideStringProvider(context: Context) =
        StringProvider(context)

}
