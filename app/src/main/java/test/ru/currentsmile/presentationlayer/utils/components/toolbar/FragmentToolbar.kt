package test.ru.currentsmile.presentationlayer.utils.components.toolbar

import android.view.View.OnClickListener
import androidx.annotation.IdRes
import androidx.annotation.StringRes

class FragmentToolbar(
    @IdRes val resId: Int,
    @StringRes val title: Int,
    val backIcon: Boolean,
    val navigationListener: OnClickListener?) {

    companion object {
        const val NO_TOOLBAR = -1
    }

    class Builder {
        private var resId: Int = -1
        private var title: Int = -1
        private var backIcon: Boolean = false
        private var navigationListener: OnClickListener? = null

        fun withId(@IdRes resId: Int) = apply { this.resId = resId }

        fun withTitle(title: Int) = apply { this.title = title }

        fun withBackNavigation(listener: OnClickListener?) = apply {
            backIcon = true
            navigationListener = listener
        }

        fun build() = FragmentToolbar(
            resId,
            title,
            backIcon,
            navigationListener
        )
    }
}