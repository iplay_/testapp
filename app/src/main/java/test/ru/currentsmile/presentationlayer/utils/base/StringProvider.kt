package test.ru.currentsmile.presentationlayer.utils.base

import android.content.Context
import androidx.annotation.StringRes

class StringProvider constructor(private val context : Context) {

    fun getString(@StringRes stringId : Int): String = context.getString(stringId)

    fun getString(@StringRes stringId : Int, vararg formatArgs : Any): String {
        return context.getString(stringId, formatArgs)
    }

}