package test.ru.currentsmile.presentationlayer.ui.base

import android.os.Bundle
import ru.terrakok.cicerone.Navigator
import ru.terrakok.cicerone.NavigatorHolder
import test.ru.currentsmile.TestApplication
import javax.inject.Inject

abstract class BaseActivity : MvpAppCompatActivity() {

    @Inject lateinit var navigatorHolder: NavigatorHolder
    lateinit var navigator: Navigator

    abstract fun initNavigation()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        TestApplication.applicationComponent.inject(this)
        initNavigation()
    }

    override fun onResumeFragments() {
        super.onResumeFragments()
        navigatorHolder.setNavigator(navigator)
    }

    override fun onPause() {
        navigatorHolder.removeNavigator()
        super.onPause()
    }

}