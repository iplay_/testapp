package test.ru.currentsmile.presentationlayer.di.module

import dagger.Module
import dagger.Provides
import dagger.Reusable
import ru.terrakok.cicerone.Cicerone
import ru.terrakok.cicerone.Router
import test.ru.currentsmile.presentationlayer.di.scope.AppScope

@AppScope
@Module
@Suppress("unused")
object NavigationModule {

    private val cicerone: Cicerone<Router> = Cicerone.create()

    @Provides
    @Reusable
    @JvmStatic
    fun provideRouter() = cicerone.router

    @Provides
    @Reusable
    @JvmStatic
    fun provideNavigatorHolder() = cicerone.navigatorHolder

}