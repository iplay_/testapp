package test.ru.currentsmile.presentationlayer.di.module

import android.content.Context
import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.Reusable
import test.ru.currentsmile.datalayer.source.LuxSourceGlobalRepository
import test.ru.currentsmile.datalayer.source.local.LuxLocalRepository
import test.ru.currentsmile.datalayer.source.remote.ILuxDataSourceApi
import test.ru.currentsmile.datalayer.source.remote.LuxSourceRemoteRepository
import test.ru.currentsmile.domainlayer.source.ILuxSourceGlobalRepository
import test.ru.currentsmile.domainlayer.source.ILuxSourceLocalRepository
import test.ru.currentsmile.domainlayer.source.ILuxSourceRemoteRepository
import test.ru.currentsmile.presentationlayer.di.scope.AppScope

@AppScope
@Suppress("unused")
@Module(includes = [(NetworkModule::class)])
object RepositoryModule {

    @Provides
    @Reusable
    @JvmStatic
    fun provideGson() = Gson()

    @Provides
    @Reusable
    @JvmStatic
    fun provideILuxSourceRemoteRepository(network: ILuxDataSourceApi): ILuxSourceRemoteRepository {
        return LuxSourceRemoteRepository(network)
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideILuxSourceGlobalRepository(scheduleDataSourceRemote: ILuxSourceRemoteRepository): ILuxSourceGlobalRepository {
        return LuxSourceGlobalRepository(scheduleDataSourceRemote)
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideLuxLocalRepository(context: Context) : LuxLocalRepository {
        return LuxLocalRepository(context)
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideILuxSourceLocalRepository(luxLocalRepository: LuxLocalRepository ): ILuxSourceLocalRepository {
        return luxLocalRepository
    }


}