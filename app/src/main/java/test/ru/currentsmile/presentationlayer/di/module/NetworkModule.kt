package test.ru.currentsmile.presentationlayer.di.module

import com.google.gson.Gson
import dagger.Module
import dagger.Provides
import dagger.Reusable
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import test.ru.currentsmile.BuildConfig
import test.ru.currentsmile.datalayer.source.remote.ILuxDataSourceApi
import test.ru.currentsmile.presentationlayer.di.scope.AppScope
import java.util.concurrent.TimeUnit

@AppScope
@Module
@Suppress("unused")
object NetworkModule {

    private const val NETWORK_TIMEOUT_TIME = 1L

    @Provides
    @Reusable
    @JvmStatic
    fun provideOkHttpClient(): OkHttpClient {
        val okHttpBuilder = OkHttpClient.Builder()
        okHttpBuilder.connectTimeout(NETWORK_TIMEOUT_TIME, TimeUnit.MINUTES)
        okHttpBuilder.writeTimeout(NETWORK_TIMEOUT_TIME, TimeUnit.MINUTES)
        okHttpBuilder.readTimeout(NETWORK_TIMEOUT_TIME, TimeUnit.MINUTES)

        val httpLoggingInterceptor = HttpLoggingInterceptor()
        if (BuildConfig.DEBUG) {
            httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
        }

        okHttpBuilder.addInterceptor(httpLoggingInterceptor)
        return okHttpBuilder.build()
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideRetrofit(okHttpClient: OkHttpClient): Retrofit {
        return Retrofit.Builder()
            .client(okHttpClient)
            .addConverterFactory(GsonConverterFactory.create(Gson()))
            .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
            .baseUrl(BuildConfig.HOST_URL)
            .build()
    }

    @Provides
    @Reusable
    @JvmStatic
    fun provideApi(retrofit: Retrofit): ILuxDataSourceApi = retrofit.create(ILuxDataSourceApi::class.java)

}