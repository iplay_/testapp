package test.ru.currentsmile.presentationlayer.ui.main

import android.os.Bundle
import androidx.recyclerview.widget.DiffUtil.DiffResult
import androidx.recyclerview.widget.LinearLayoutManager
import com.arellomobile.mvp.presenter.InjectPresenter
import test.ru.currentsmile.presentationlayer.presenter.main.IMain
import test.ru.currentsmile.presentationlayer.presenter.main.MainPresenter
import test.ru.currentsmile.presentationlayer.ui.base.BaseFragment
import kotlinx.android.synthetic.main.fragment_main.mainLayout
import kotlinx.android.synthetic.main.fragment_main.mainList
import kotlinx.android.synthetic.main.fragment_main.mainProgress
import kotlinx.android.synthetic.main.fragment_main.mainSearchInput
import kotlinx.android.synthetic.main.fragment_main.mainSearchInputClear
import kotlinx.android.synthetic.main.include_toolbar.defaultToolbar
import test.ru.currentsmile.R
import test.ru.currentsmile.R.layout
import test.ru.currentsmile.presentationlayer.ui.main.adapter.MainAdapter
import test.ru.currentsmile.presentationlayer.utils.components.toolbar.FragmentToolbar
import test.ru.currentsmile.presentationlayer.utils.extension.showSnack
import test.ru.currentsmile.presentationlayer.utils.extension.visible

class MainFragment : BaseFragment(), IMain {

    @InjectPresenter lateinit var presenter: MainPresenter
    private lateinit var adapter: MainAdapter

    override val setLayout = layout.fragment_main

    override fun onActivityCreated(savedInstanceState: Bundle?) {
        super.onActivityCreated(savedInstanceState)

        adapter = MainAdapter(presenter.list, presenter)
        mainList.layoutManager = LinearLayoutManager(context!!)
        mainList.adapter = adapter

        mainSearchInputClear.setOnClickListener { clearSearch() }

        presenter.setupSearch(mainSearchInput)
    }

    override fun updateState(hintText: String) {
        defaultToolbar.title = hintText
        mainSearchInput.hint = hintText
    }

    override fun clearSearch() {
        mainSearchInput.text = null
    }

    override fun updateList(diffResult: DiffResult) {
        diffResult.dispatchUpdatesTo(adapter)
    }

    override fun showMessage(message: String) {
        mainLayout.showSnack(message)
    }

    override fun changeProgress(isVisible: Boolean) {
        mainProgress.visible(isVisible)
        mainList.visible(!isVisible)
    }

    override fun onBackPressed(): Boolean {
        return false
    }

    override fun builder(): FragmentToolbar {
        return FragmentToolbar.Builder()
            .withId(R.id.defaultToolbar)
            .withTitle(R.string.main_screen_title)
            .build()
    }

}