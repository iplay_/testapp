package test.ru.currentsmile.presentationlayer.ui.main

import android.content.Intent
import android.os.Bundle
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentTransaction
import ru.terrakok.cicerone.android.support.SupportAppNavigator
import ru.terrakok.cicerone.commands.Command
import test.ru.currentsmile.R.layout
import test.ru.currentsmile.presentationlayer.ui.base.BackButtonListener
import test.ru.currentsmile.presentationlayer.ui.base.BaseActivity
import test.ru.currentsmile.presentationlayer.navigation.Screens
import ru.terrakok.cicerone.commands.Replace
import test.ru.currentsmile.R.id

class MainActivity : BaseActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(layout.activity_main)

        if (savedInstanceState == null) {
            navigator.applyCommands(arrayOf<Command>(Replace(Screens.MainScreen())))
        }
    }

    override fun initNavigation() {
        navigator =
            object : SupportAppNavigator(this, supportFragmentManager, id.navigationContainer) {
                override fun setupFragmentTransaction(
                    command: Command?,
                    currentFragment: Fragment?,
                    nextFragment: Fragment?,
                    fragmentTransaction: FragmentTransaction?
                ) {}
            }
    }

    override fun onBackPressed() {
        val fm = supportFragmentManager
        var fragment: Fragment? = null
        val fragments = fm.fragments
        for (f in fragments) {
            if (f.isVisible) {
                fragment = f
                break
            }
        }
        if (fragment != null && fragment is BackButtonListener && (fragment as BackButtonListener).onBackPressed()) {
            return
        } else {
            super.onBackPressed()
        }
    }

}
