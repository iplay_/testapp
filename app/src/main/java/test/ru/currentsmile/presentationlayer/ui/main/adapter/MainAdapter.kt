package test.ru.currentsmile.presentationlayer.ui.main.adapter

import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.appcompat.widget.AppCompatTextView
import androidx.cardview.widget.CardView
import androidx.recyclerview.widget.RecyclerView
import test.ru.currentsmile.R
import test.ru.currentsmile.presentationlayer.model.main.CitySealedModel

class MainAdapter constructor(val list: List<CitySealedModel>, val listener: MainAdapterListener) :
    RecyclerView.Adapter<RecyclerView.ViewHolder>() {

    companion object {
        private const val TYPE_HEADER = 1
        private const val TYPE_PLACE = 2
    }

    interface MainAdapterListener {
        fun onCitySelected(position: Int)
    }
    override fun getItemViewType(position: Int): Int {
        return when (list[position]) {
            is CitySealedModel.Title -> TYPE_HEADER
            else -> TYPE_PLACE
        }
    }

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): RecyclerView.ViewHolder {
        return when (viewType) {
            TYPE_HEADER -> {
                HeaderViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_title, parent, false
                    )
                )
            }
            else -> {
                CityViewHolder(
                    LayoutInflater.from(parent.context).inflate(
                        R.layout.item_city, parent, false
                    )
                )
            }
        }
    }

    override fun onBindViewHolder(noneHolder: RecyclerView.ViewHolder, position: Int) {
        val viewType = noneHolder.itemViewType
        val model = list[noneHolder.adapterPosition]
        if (viewType == TYPE_HEADER) {
            val data = model as CitySealedModel.Title
            (noneHolder as HeaderViewHolder).itemTitleLayout.text = data.title
        } else {
            val data = model as CitySealedModel.CityModel
            (noneHolder as CityViewHolder).itemPlaceTitle.text = data.name
            noneHolder.itemPlaceHotels.text = data.hotels
        }
    }

    override fun getItemCount() = list.size

    inner class HeaderViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView) {
        val itemTitleLayout: AppCompatTextView = itemView.findViewById(R.id.itemTitleLayout)
    }

    inner class CityViewHolder constructor(itemView: View) : RecyclerView.ViewHolder(itemView), View.OnClickListener {
        private val itemPlaceLayout: CardView = itemView.findViewById(R.id.itemPlaceLayout)
        val itemPlaceTitle: AppCompatTextView = itemView.findViewById(R.id.itemPlaceTitle)
        val itemPlaceHotels: AppCompatTextView = itemView.findViewById(R.id.itemPlaceHotels)

        init {
            itemPlaceLayout.setOnClickListener(this)
        }

        override fun onClick(v: View?) {
            val position = adapterPosition
            if (position != RecyclerView.NO_POSITION) {
                listener.onCitySelected(position)
            }
        }
    }

}