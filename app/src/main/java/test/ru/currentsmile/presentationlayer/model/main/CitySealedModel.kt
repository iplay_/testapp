package test.ru.currentsmile.presentationlayer.model.main

import test.ru.currentsmile.domainlayer.entity.response.CityLocationEntity

sealed class CitySealedModel {

    data class CityModel(
        val id: Long,
        val name: String,
        val city: String,
        val location: CityLocationEntity,
        val hotels: String
    ) : CitySealedModel()

    data class Title(val title: String) : CitySealedModel()
}