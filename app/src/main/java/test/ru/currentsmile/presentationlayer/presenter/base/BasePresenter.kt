package test.ru.currentsmile.presentationlayer.presenter.base

import com.arellomobile.mvp.MvpPresenter
import io.reactivex.Observable
import io.reactivex.Single
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.internal.util.NotificationLite.getError
import retrofit2.HttpException
import ru.terrakok.cicerone.Router
import ru.terrakok.cicerone.Screen
import test.ru.currentsmile.BuildConfig
import test.ru.currentsmile.R
import test.ru.currentsmile.TestApplication
import test.ru.currentsmile.presentationlayer.utils.base.StringProvider
import java.io.IOException
import java.net.SocketTimeoutException
import javax.inject.Inject

open class BasePresenter<View : BaseMvpView> : MvpPresenter<View>() {

    private val mComposite = CompositeDisposable()

    @Inject open lateinit var router: Router
    @Inject open lateinit var stringProvider: StringProvider

    override fun onDestroy() {
        super.onDestroy()
        mComposite.dispose()
    }

    fun go(screen: Screen) {
        router.navigateTo(screen)
    }

    fun goBack() {
        router.exit()
    }

    fun replace(screenName: Screen) {
        router.replaceScreen(screenName)
    }

    fun goBackTo(screenName: Screen) {
        router.backTo(screenName)
    }

    internal fun Disposable.connect() {
        mComposite.add(this)
    }

    internal fun onError(t: Throwable) {
        if (BuildConfig.DEBUG) t.printStackTrace()
        when (t) {
            is HttpException -> {
                viewState?.showMessage(stringProvider.getString(R.string.server_placeholder))
            }
            is SocketTimeoutException -> viewState?.showMessage(stringProvider.getString(R.string.server_placeholder))
            is IOException -> viewState?.showMessage(stringProvider.getString(R.string.internet_placeholder))
            else -> viewState?.showMessage(stringProvider.getString(R.string.server_placeholder))
        }
    }

    internal fun <T> Single<T>.progress(): Single<T> {
        return this.doOnSubscribe { viewState?.changeProgress(true) }
            .doAfterTerminate { viewState?.changeProgress(false) }
    }

    internal fun <T> Observable<T>.progress(): Observable<T> {
        return this.doOnSubscribe { viewState?.changeProgress(true) }
            .doAfterTerminate { viewState?.changeProgress(false) }
    }

    fun getComponent() = TestApplication.applicationComponent

}