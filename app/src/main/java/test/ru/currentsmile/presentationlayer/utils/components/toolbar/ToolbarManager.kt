package test.ru.currentsmile.presentationlayer.utils.components.toolbar

import android.view.View
import androidx.appcompat.widget.Toolbar
import test.ru.currentsmile.R

class ToolbarManager constructor(
    private var builder: FragmentToolbar,
    private var container: View) {

    companion object {
        const val NO_FUNC = -1
    }

    fun prepareToolbar() {
        if (builder.resId != FragmentToolbar.NO_TOOLBAR) {
            val fragmentToolbar = container.findViewById(builder.resId) as Toolbar

            if (builder.title != NO_FUNC) {
                fragmentToolbar.setTitle(builder.title)
            }

            if (builder.navigationListener != null) {
                fragmentToolbar.setNavigationIcon(R.drawable.ic_chevron_left_white_24dp)
                fragmentToolbar.setNavigationOnClickListener(builder.navigationListener)
            }
        }
    }
}