package test.ru.currentsmile.presentationlayer.ui.base

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.annotation.LayoutRes
import test.ru.currentsmile.presentationlayer.utils.components.toolbar.FragmentToolbar
import test.ru.currentsmile.presentationlayer.utils.components.toolbar.ToolbarManager

abstract class BaseFragment : MvpAppCompatFragment(), BackButtonListener {

    @get:LayoutRes protected abstract val setLayout: Int
    protected abstract fun builder(): FragmentToolbar

    override fun onCreateView(inflater: LayoutInflater, container: ViewGroup?, savedInstanceState: Bundle?): View  {
        if (setLayout == 0) {
            throw RuntimeException("Invalid Layout ID")
        }

        return inflater.inflate(setLayout, container, false)
    }

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        ToolbarManager(builder(), view).prepareToolbar()
    }

}