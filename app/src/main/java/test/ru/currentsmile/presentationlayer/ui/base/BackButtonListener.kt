package test.ru.currentsmile.presentationlayer.ui.base

interface BackButtonListener {
    fun onBackPressed(): Boolean
}