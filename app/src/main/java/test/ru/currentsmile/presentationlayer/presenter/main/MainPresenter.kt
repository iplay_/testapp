package test.ru.currentsmile.presentationlayer.presenter.main

import androidx.appcompat.widget.AppCompatEditText
import androidx.recyclerview.widget.DiffUtil
import com.arellomobile.mvp.InjectViewState
import com.arellomobile.mvp.MvpFacade.init
import io.reactivex.android.schedulers.AndroidSchedulers
import test.ru.currentsmile.R
import test.ru.currentsmile.datalayer.util.RxUtils
import test.ru.currentsmile.domainlayer.interactor.MainInteractor
import test.ru.currentsmile.presentationlayer.model.diff.CityDiffUtilCallback
import test.ru.currentsmile.presentationlayer.model.main.CitySealedModel
import test.ru.currentsmile.presentationlayer.model.main.CitySealedModel.CityModel
import test.ru.currentsmile.presentationlayer.model.mapper.MainMapper
import test.ru.currentsmile.presentationlayer.presenter.base.BasePresenter
import test.ru.currentsmile.presentationlayer.ui.main.adapter.MainAdapter
import test.ru.currentsmile.presentationlayer.utils.RxSearchObservable
import java.util.concurrent.TimeUnit
import javax.inject.Inject

@InjectViewState
class MainPresenter : BasePresenter<IMain>(), MainAdapter.MainAdapterListener {

    companion object {
        private const val SEARCH_DELAY = 350L
    }

    @Inject
    internal lateinit var interactor: MainInteractor
    private var mapper: MainMapper
    private var isOrigin = true
    private lateinit var from: CityModel
    private lateinit var to: CityModel

    val list = arrayListOf<CitySealedModel>()

    override fun attachView(view: IMain?) {
        super.attachView(view)
        viewState?.updateState(getSearchHint())
    }

    fun setupSearch(input: AppCompatEditText) {
        RxSearchObservable.fromView(input)
            .debounce(SEARCH_DELAY, TimeUnit.MILLISECONDS, AndroidSchedulers.mainThread())
            .compose(RxUtils.toObservable())
            .switchMapSingle {
                viewState?.changeProgress(true)
                interactor.getPlaces(place = it, isFromCache = false)
            }
            .map {
                if (it.cities.isNotEmpty()) {
                    mapper.map(it.cities, isDestination = !isOrigin)
                } else {
                    arrayListOf()
                }
            }
            .subscribe({
                val diffUtilCallback = CityDiffUtilCallback(list, it)
                val diffResult = DiffUtil.calculateDiff(diffUtilCallback)

                list.clear()
                list.addAll(it)
                viewState?.updateList(diffResult)
                viewState?.changeProgress(false)
            }, {
                onError(it)
                viewState?.changeProgress(true)
            })
            .connect()
    }

    override fun onCitySelected(position: Int) {
        if (isOrigin) {
            from = list[position] as CityModel
            isOrigin = !isOrigin
            list.clear()
            viewState?.updateState(getSearchHint())
            viewState?.clearSearch()
        } else {
            to = list[position] as CityModel
            viewState?.updateState(
                String.format(
                    stringProvider.getString(R.string.main_filled_title),
                    from.city,
                    to.city
                )
            )
        }
    }

    private fun getSearchHint() = if (isOrigin) {
        stringProvider.getString(R.string.main_type_origin)
    } else {
        stringProvider.getString(R.string.main_type_destination)
    }

    init {
        getComponent().inject(this)
        mapper = MainMapper(stringProvider)
    }
}