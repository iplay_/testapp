package test.ru.currentsmile.domainlayer.interactor

import io.reactivex.Single
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity

interface IMainInteractor {

    fun getPlaces(place: String, isFromCache: Boolean) : Single<PlacesEntity>

}