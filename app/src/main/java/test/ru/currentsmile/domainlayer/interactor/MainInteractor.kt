package test.ru.currentsmile.domainlayer.interactor

import io.reactivex.Single
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity
import test.ru.currentsmile.domainlayer.source.ILuxSourceGlobalRepository
import javax.inject.Inject

class MainInteractor @Inject constructor(private val globalRepository: ILuxSourceGlobalRepository) : IMainInteractor {

    override fun getPlaces(place: String, isFromCache: Boolean): Single<PlacesEntity> {
        return globalRepository.getPlaces(place = place, isFromCache = isFromCache)
    }
}