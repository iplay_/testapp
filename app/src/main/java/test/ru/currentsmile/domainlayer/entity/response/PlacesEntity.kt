package test.ru.currentsmile.domainlayer.entity.response

data class PlacesEntity(val cities: List<CityEntity>)