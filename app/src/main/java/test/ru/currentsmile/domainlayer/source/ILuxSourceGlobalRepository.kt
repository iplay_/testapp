package test.ru.currentsmile.domainlayer.source

import io.reactivex.Single
import retrofit2.http.Path
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity

interface ILuxSourceGlobalRepository {

    /* Main */
    fun getPlaces(place: String, isFromCache: Boolean) : Single<PlacesEntity>

}