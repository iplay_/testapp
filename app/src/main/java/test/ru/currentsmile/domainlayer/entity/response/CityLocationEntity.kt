package test.ru.currentsmile.domainlayer.entity.response

data class CityLocationEntity(val lat: Double, val lon: Double)