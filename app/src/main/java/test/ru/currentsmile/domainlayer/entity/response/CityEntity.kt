package test.ru.currentsmile.domainlayer.entity.response

data class CityEntity(
    val id: Long,
    val fullname: String,
    val city: String,
    val location: CityLocationEntity,
    val hotelsCount: Long
)