package test.ru.currentsmile.domainlayer.source

import io.reactivex.Single
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity

interface ILuxSourceRemoteRepository {

    /* Main */
    fun getPlaces(place: String) : Single<PlacesEntity>

}