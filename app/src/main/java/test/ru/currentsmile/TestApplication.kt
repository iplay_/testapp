package test.ru.currentsmile

import android.app.Application
import test.ru.currentsmile.presentationlayer.di.component.ApplicationComponent
import test.ru.currentsmile.presentationlayer.di.component.DaggerApplicationComponent
import test.ru.currentsmile.presentationlayer.di.module.ContextModule
import test.ru.currentsmile.presentationlayer.di.module.NavigationModule
import test.ru.currentsmile.presentationlayer.di.module.NetworkModule
import test.ru.currentsmile.presentationlayer.di.module.RepositoryModule

class TestApplication : Application() {

    companion object {
        @JvmStatic
        lateinit var applicationComponent: ApplicationComponent
            private set
    }

    override fun onCreate() {
        super.onCreate()
        applicationComponent = buildComponent()
    }

    private fun buildComponent(): ApplicationComponent {
        return DaggerApplicationComponent.builder()
            .contextModule(ContextModule(this))
            .networkModule(NetworkModule)
            .navigationModule(NavigationModule)
            .repositoryModule(RepositoryModule)
            .build()
    }

}