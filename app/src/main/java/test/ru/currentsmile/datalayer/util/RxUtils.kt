package test.ru.currentsmile.datalayer.util

import io.reactivex.ObservableTransformer
import io.reactivex.SingleTransformer

object RxUtils {

    fun <T> toSingle(): SingleTransformer<T, T> = RxUtilsSingle.applyIOToMainThreadSchedulers<T>()
    fun <T> toObservable(): ObservableTransformer<T, T> = RxUtilsObservable.applyIOToMainThreadSchedulers<T>()

}