package test.ru.currentsmile.datalayer.source

import io.reactivex.Single
import test.ru.currentsmile.datalayer.util.RxUtils
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity
import test.ru.currentsmile.domainlayer.source.ILuxSourceGlobalRepository
import test.ru.currentsmile.domainlayer.source.ILuxSourceRemoteRepository

class LuxSourceGlobalRepository constructor(private val remote: ILuxSourceRemoteRepository) :
    ILuxSourceGlobalRepository {

    override fun getPlaces(place: String, isFromCache: Boolean): Single<PlacesEntity> {
        return remote.getPlaces(place)
    }
}