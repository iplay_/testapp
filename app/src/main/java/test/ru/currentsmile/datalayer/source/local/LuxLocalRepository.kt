package test.ru.currentsmile.datalayer.source.local

import android.content.Context
import android.content.SharedPreferences
import test.ru.currentsmile.domainlayer.source.ILuxSourceLocalRepository
import javax.inject.Inject

class LuxLocalRepository @Inject constructor(context: Context) : ILuxSourceLocalRepository {

    companion object {
        private const val PREFS_FILENAME = "luxsoft.test.app.prefs"

        private const val PREFS_DATE_TIME = "prefs_date_time"
    }

    private val prefs: SharedPreferences = context.getSharedPreferences(PREFS_FILENAME, 0)

    override var lastUpdateTime: String
        get() = prefs.getString(PREFS_DATE_TIME, "")!!
        set(value) {
            prefs.edit().apply {
                putString(PREFS_DATE_TIME, value)
                apply()
            }
        }
}