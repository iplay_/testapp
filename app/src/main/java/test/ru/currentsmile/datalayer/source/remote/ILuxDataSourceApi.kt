package test.ru.currentsmile.datalayer.source.remote

import io.reactivex.Single
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity

interface ILuxDataSourceApi {

    @GET("autocomplete?lang=ru")
    fun getPlaces(@Query("term") place: String) : Single<PlacesEntity>

}