package test.ru.currentsmile.datalayer.source.remote

import io.reactivex.Single
import test.ru.currentsmile.datalayer.util.RxUtils
import test.ru.currentsmile.domainlayer.entity.response.PlacesEntity
import test.ru.currentsmile.domainlayer.source.ILuxSourceRemoteRepository
import java.util.concurrent.TimeUnit.SECONDS

class LuxSourceRemoteRepository constructor(private val networkApi: ILuxDataSourceApi) : ILuxSourceRemoteRepository {

    override fun getPlaces(place: String): Single<PlacesEntity> {
        return networkApi.getPlaces(place = place).compose(RxUtils.toSingle())
    }
}